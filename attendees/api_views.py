from django.http import JsonResponse

from .models import Attendee

from events.models import Conference

#recheck this one later seems clunky
from events.api_views import ConferenceEncoder

from common.json import ModelEncoder

from django.views.decorators.http import require_http_methods

import json


# list attendee encoder
class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]



@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
  
        return JsonResponse(
                    {"attendees": attendees},
                    encoder=AttendeeListEncoder,
                    safe=False,
                )
    else:
        content = json.loads(request.body)

    try:
        conference = Conference.objects.get(id=conference_id)
        content["conference"] = conference
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )


# detail attendee encoder finished

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceEncoder(),
    }

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse(({"deleted": count > 0}))
    
    else:   # PUT
        # copied from create
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(conference=content["conference"])
                content["conference"] = conference
        
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference"},
                status=400,
            )
        
        Attendee.objects.filter(id=id).update(**content)
        atendee = Attendee.objects.get(id=id)
        return JsonResponse(
            atendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )