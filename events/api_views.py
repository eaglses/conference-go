from django.http import JsonResponse

from .models import Conference, Location, State

from common.json import ModelEncoder

from django.views.decorators.http import require_http_methods

import json

from .acls import get_photo, get_weather_data
# multi use encoder

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name"
        ]









#_____list Conferences __ unfinished per instructions

class ConferenceEncoder(ModelEncoder):
    model = Conference
    properties = [
         "name",
     ]    


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
        if request.method == "GET":
            conferences = Conference.objects.all()
            return JsonResponse(
                {"conferences": conferences},
                encoder=ConferenceEncoder,
                safe=False,
            )
        else:
            content = json.loads(request.body)

           
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceEncoder,
            safe=False,
        )



#_____ detail conference__ unfinished

class ConferenceEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


def api_show_conference(request, id):
   
    conference = Conference.objects.get(id=id)

    weather = get_weather_data(conference.location.city, conference.location.state)

    return JsonResponse(
        {"conference": conference, "weather": weather},
        encoder=ConferenceEncoder,
        safe=False,
    )


#____list locations unfinished "LocationListEncoder" ___example per instructions VVV

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "picture_url"
        ]
    

@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse( 
            {"locations":locations},
            encoder=LocationListEncoder,
            safe=False,
    )
    else:
        content = json.loads(request.body)

        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        photo = get_photo(content["city"], content["state"].abbreviation)
        content.update(photo)
        location = Location.objects.create(**content)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )


#_____ location detail- unfinished

class StateDetailEncoder(ModelEncoder):
    model = State
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
        "picture_url",
    ]
    encoders = {
        "state": StateDetailEncoder(),
    }

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        conference = Location.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=LocationDetailEncoder,
            safe=False,        
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse(({"deleted": count > 0}))
    else:   # PUT 
        # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # new code
        Location.objects.filter(id=id).update(**content)

        # copied from get detail
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


