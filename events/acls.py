from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

#instructor demonstrated version
def get_photo(city, state):
    print(PEXELS_API_KEY)
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + " " + state, "per_page": 1}
    response = requests.get(url, params=params, headers=headers)
    print(response)
    content = json.loads(response.content)
    
    return {"picture_url": content["photos"][0]["src"]["original"]}


def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{1}&limit={1}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    content = json.loads(response.content)
    latitude = content[0]["lat"]
    lon = content[0]["lon"]
    print("lat", latitude, "lon", lon)

    # Create the URL for the current weather API with the latitude
    weatherUrl = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    responseWeather = requests.get(weatherUrl)
    # Parse the JSON response
    contentWeather = json.loads(responseWeather.content)
    return {"weather": contentWeather["weather"][0]["main"], "temp": contentWeather["main"]["temp"]}